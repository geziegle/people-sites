# People sites

## Purpose
Determine which people.ucsc.edu sites are still active and which are not using HTTP requests.

## Term Definitions
Request: A HTTP message sent from a client to a server, in this case the server is hosted at people.ucsc.edu
Response: A HTTP message sent from a server to a client, in response to a request.
Status Code: A 3-digit number returned in the HTTP response that indicates status of the site.
Active Site: A website of URL: "people.ucsc.edu/~[cruzid]" that returns a 200 status code in the response to an HTTP get request. This active site could be:
1. A human-made HTML Page
2. A rendered HTML page which illustrates a file directory
3. A HTML page containing a redirect

## Revision History


## Script Name
For accessing a remote google sheet, use:
`python getURLsFromSpreadsheet.py`

For accessing a local file in .csv format, use:
`python checkURLsFromCSV.py [path to local file]`

## Git Repo Location
https://git.ucsc.edu/geziegle/people-sites

## How to interpret the CSV output
The script will check the url from each line, and it will output a http status (with some interpretation) to the CSV file, here is a tree of how the outcomes are reached

```
HTTP Request sent to site
├── 403 - Site public access is forbidden
├── 404 - Site has not contents
├── 200 - Something exists on the site, check for redirect or directory
│   ├── Site exists (active, non-directory site)
│   ├── Redirect
│   ├── Directory, check for contents
│   │   ├── Populated Directory
│   │   ├── Empty Directory
```
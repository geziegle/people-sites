import sys
from csv import reader
import requests
import configparser

# This will be run when the file is executed
if __name__ == "__main__":
    # Parse the arguments
    print(f"Args count: {len(sys.argv)}")
    for i, arg in enumerate(sys.argv):
        print(f"Argument {i:>6}: {arg}")

    # Open the output file for writing
    outFile = open("CSVout.csv", "w+")

    activeCount = 0

    # open file in read mode
    with open(sys.argv[1], 'r') as read_obj:
        # pass the file object to reader() to get the reader object
        csv_reader = reader(read_obj)
        # write column headers
        outFile.write("~cruzID,,,cruzID,url,http status code,http status message,[Directory Only] contents,\n")
        # Iterate over each row in the csv using reader object
        for row in csv_reader:
            # The url is the 5th item in each list
            url = row[4]
            # if the row doesn't have a url, skip it
            if url == '' or url == 'URL':
                continue
            print (url, end=": ")
            # send a header request to the url
            response = requests.get(url, allow_redirects=True)
            if (response.status_code != 404): activeCount += 1
            # if (response.status_code != 404): print (response.text)
            # if (response.url != url): print (response.url)
            print("\t", response.status_code, end ="\t")
            # Save the status code
            row[5] = str(response.status_code)
            # Response codes will be one of two options (per the library)
            # 200 and 404
            # 404s are 404s, but anything else could be
            # A Page
            # A directory (rendered html)
            if response.status_code == 403:
                print("Forbidden")
                row.append("Forbidden")
            elif response.status_code == 404:
                print("Not Found")
                row.append("Not Found")
            else:
                # Check for <h1>Index of [URL]</h1>
                checkTitle = "<title>Index of /" + row[0] + "</title>"
                checkHeading = "<h1>Index of /" + row[0] + "</h1>"
                if checkTitle in response.text and checkHeading in response.text:
                    # Say it's likely a directory
                    print("Likely a directory", end=", ")
                    # Check if the directory is empty
                    trNum = response.text.count("<tr>")
                    print (trNum, end=' Rows in table: ')
                    if trNum <= 4:
                        print ("Empty Directory")
                        row.append("Directory,Empty")
                    else:
                        print ("Populated Directory")
                        row.append("Directory,Populated")
                # Otherwise check for refresh tag
                elif """http-equiv=\"refresh\"""".lower() in response.text.lower():
                    row.append("Redirect,http redirect")
                    print ("Likely a redirect")
                else:
                    print()
                    row.append("active non-directory site,")
            
            # A Redirect
            # has <meta http-equiv="Refresh" content="0; url='https://rfairlie.sites.ucsc.edu'" />
            #     <meta http-equiv="refresh" content="1; URL=https://www.akiladesilva.com/">
            # <meta> tags only work in the head object (<head></head>)
            # Write output
            for item in row:
                outFile.write(item + ",")
            outFile.write("\n")

    # Print number of active sites
    print ("Active Sites:", activeCount)
